RV monitor automata models powered by eBPF tracing

This is an experiment that implements the RV monitor automata models
to enforce deterministic execution flow execution with eBPF tracing.

These eBPF powered automata models:

- are self contained: there's no repetitive coding work required at
  every extension and regeneration of the automata model

- cannot crash the kernel or corrupt memory, regardless of how many
  mistakes and bugs are introduced in the source code of the automata
  model, thanks to the eBPF verifier

- they can be upgraded and extended on the live system, without
  requiring reboot and without requiring secret keys to sign new
  kernel modules, even if secure boot and kernel lockdown in integrity
  mode are both enabled

These properties could make the eBPF powered implementation of the
deterministic execution flow control automata models preferable at the
edge where a risk of kernel crash or memory corruption caused by a bug
in the Runtime Verification monitoring kernel modules cannot be
tolerated.

This has only the equivalent capability of the printk RV reactor. To
be functionally equivalent in terms of RV reactors, we need to
introduce a eBPF panic() call to eBPF tracing hooks, so bpftrace and
bcc-tools can also invoke panic(), if bpftrace is invoked with
--allow-panic (or --unsafe). That new panic feature will not be useful
just for the RV monitoring use case, but it will also help to trigger
a kdump as soon as possible after any broken kernel invariant has been
detected in the hope to obtain more meaningful debug data in the
kdump. So it's generally a good feature to add to eBPF tracing in the
future.
